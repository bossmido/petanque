-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mar 20 Juin 2017 à 10:00
-- Version du serveur :  10.0.29-MariaDB-0ubuntu0.16.04.1
-- Version de PHP :  7.0.18-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `petanque`
--

-- --------------------------------------------------------

--
-- Structure de la table `equipe`
--

use petanque;

-- --------------------------------------------------------



--
-- Structure de la table `type`
--

CREATE TABLE IF NOT EXISTS `type` (
  `id_partie_type` int(32) NOT NULL AUTO_INCREMENT,
  `libelle_type` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id_partie_type`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

--
-- Vider la table avant d'insérer `type`
--


 INSERT IGNORE into `type` (`id_partie_type`,`libelle_type`) values(1,'mélée'); 
 INSERT IGNORE into `type` (`id_partie_type`,`libelle_type`) values(2,'normal'); 

 INSERT IGNORE into `partie` (`id_partie_Entite`,`score_1_Entite`,`score_2_Entite`) values(1,3,4); 

 INSERT IGNORE into `partie` (`id_partie_Entite`,`score_1_Entite`,`score_2_Entite`) values(2,3,4); 




-- TRUNCATE TABLNcture de la table `tournoi`
--

CREATE TABLE IF NOT EXISTS `tournoi` (
  `id_tournoi_tournoi` int(32) NOT NULL AUTO_INCREMENT,
  `nombre_partie_tournoi` int(32) DEFAULT NULL,
  `date_tournoi` date DEFAULT NULL,
  `nom_tournoi_tournoi` varchar(32) DEFAULT NULL,
  `id_partie_type` int(32) DEFAULT NULL,
  PRIMARY KEY (`id_tournoi_tournoi`),
  KEY `FK_tournoi_id_partie_type` (`id_partie_type`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

--

-- TRUNCATE TABLE `tournoi`;
--
-- Contenu de la table `tournoi`
--

 INSERT IGNORE INTO `tournoi` (`id_tournoi_tournoi`, `nombre_partie_tournoi`, `date_tournoi`, `nom_tournoi_tournoi`, `id_partie_type`) VALUES
(1, 2, '2017-06-14', 'tournoi cool', 1),
(2, 3, '2017-06-07', 'tournoi autre', 2);



--
-- Structure de la table `joueur`
--

CREATE TABLE IF NOT EXISTS `joueur` (
  `id_joueur_joueur` int(32) NOT NULL AUTO_INCREMENT,
  `nom_joueur` varchar(32) DEFAULT NULL,
  `prenom_joueur` varchar(32) DEFAULT NULL,
  `sexe_joueur` enum('FEMININ','MASCULIN') DEFAULT NULL,
  `equipe_id_equipe_equipe` int(32) DEFAULT NULL,
  PRIMARY KEY (`id_joueur_joueur`),
  KEY `FK_joueur_equipe_id_equipe_equipe` (`equipe_id_equipe_equipe`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;



 INSERT IGNORE into `equipe` (`id_equipe_equipe`,`nom_equipe`) values(1,'equipe1');
 INSERT IGNORE into `equipe` (`id_equipe_equipe`,`nom_equipe`) values(2,'equipe2');


 INSERT IGNORE into `joueur` (`id_joueur_joueur`,`nom_joueur`,`prenom_joueur`,`sexe_joueur`) values(1,'joueur1','joueur1',2); 



 INSERT IGNORE into `joueur` (`id_joueur_joueur`,`nom_joueur`,`prenom_joueur`,`sexe_joueur`) values(2,'joueur2','joueur2',1);

CREATE TABLE IF NOT EXISTS `participe_à` (
  `id_equipe_equipe` int(32) NOT NULL AUTO_INCREMENT,
  `id_tournoi_tournoi` int(32) NOT NULL,
  `id_partie_Entite` int(32) NOT NULL,
  PRIMARY KEY (`id_equipe_equipe`,`id_tournoi_tournoi`,`id_partie_Entite`),
  KEY `FK_participe_à_id_tournoi_tournoi` (`id_tournoi_tournoi`),
  KEY `FK_participe_à_id_partie_Entite` (`id_partie_Entite`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

--
-- Vider la table avant d'insérer `participe_à`
--

-- TRUNCATE TABLE `participe_à`;
--
-- Contenu de la table `participe_à`
--

 INSERT IGNORE INTO `participe_à` (`id_equipe_equipe`, `id_tournoi_tournoi`, `id_partie_Entite`) VALUES
(1, 2, 2),
(2, 1, 2);

INSERT IGNORE INTO `equipe_joueur` (`id_equipe_equipe`,`id_joueur_joueur`) VALUES
(1,1),
(1,2)
;


-- Contraintes pour la table `participe_

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
