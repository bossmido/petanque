<?php
header("Content-type: text/css");
require_once(__DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."src/Configuration.php");
?>

h1{
    color : <?php print(Configuration::$couleur_titre); ?>;
}

.contenu{
    background-color : <?php print(Configuration::$couleur_fond); ?>;
    padding-bottom : 15px;
    padding-left : 4px;
    padding-right : 4px;
}

body{
    background-color :MintCream;
}

.entete{
    background-color : <?php print(Configuration::$couleur_fond_entete); ?>;
}

.footer{
    background-color : <?php print(Configuration::$couleur_fond_footer); ?>;
}

.entete{
    min-width : 300px;
    padding-top : 55px; 
    padding-bottom : 55px;
    text-align : center;
    font-weight : bold;
    font-size: 30px;
}

.ensemble{

}

.debug{
    background-color : orange;
    width : 100%;
    height : 200px;
}
<?php
if(!Configuration::$debug){
    print('.debug{diplay:none;}\n');
}
?>

.contenu{

}

.footer{
    position: fixed;
    bottom: 0;
    left:0;
    width: 100%;
}



.hidden{

	background-color : green;
	display : none;
}

.vue{
	display : block;
	background-color : red;
}

label{
	max-width : 150px;
	width : 110px;
	height : 90px;
	vertical-align : center :
	text-align : center; 
}


.label{

	height : 60px;
 	font-weight: bold;
}
.menu{
    display : flex;
}
.menu .onglet{
}
.menu a{
    display : inline-block;
    min-width : 150px;
    min-height : 90px;
}

.menu {
    text-align : center;
    justify-content: center;   
    vertical-align: middle;
    margin-top : 5px;
}

select{
    width : 120px;
    height : 35px;
}

input{
    margin-bottom :10px;
}
h1,h2,h3{
    margin-top : 4px;
    margin-left : 4px;
    font-size : 15px;
    font-family:georgia, serif;

}

a{
    font-size : 28px;  
}

a:hover{
    font-weight : 600;
    text-shadow: 2px 2px 10px #ff0000;
}

/* style table */
th{
    linear-gradient(#777, #444);
    border: 7px solid #eff;

    box-shadow: inset 0 1px 0 #999;
    color: #000;
    font-weight: bold;
    padding: 10px 15px;
    position: relative;
    background-color : #0
    text-shadow: 0 1px 0 #eff;
    
}
th:hover{
background-color : orange;
}
td {
    border-right: 1px solid #fff;
    border-left: 1px solid #e8e8e8;
    border-top: 1px solid #fff;
    border-bottom: 1px solid #e8e8e8;
    padding: 10px 15px;
    position: relative;
    transition: all 300ms;
}
table{
    background: #fff;
    border-collapse: separate;
    box-shadow: inset 0 1px 0 #fff;
    font-size: 12px;
    line-height: 24px;
    margin: 6px auto;
    text-align: left;
    width: 80%;
}
td:first-child {
    box-shadow: inset 1px 0 0 #fff;
}
