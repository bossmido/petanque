<?php
require_once 'Printable.php';
require_once 'Sauvable.php';
require_once 'Contenu.php';

// code type d'ici http://jf-lepine.developpez.com/tutoriels/php/heritage-multiple-php/
class ClasseAbstraiteRegroupement{

  private $_tExtends          = array('Sauvable','Contenu');
    private $_tExendInstances   = array();

    /**
     * Constructeur
     * création des instances de chaque classe mère
     */
    public function __construct() {
        // ::::: build instance for each parent class :::::
        foreach($this->_tExtends as $className) $this->_tExendInstances[] = new $className;
    }

    /**
     * Méthode magique __call()
     * On va reporter chaque appel sur une des instances des classes mères
     * @param string $funcName
     * @param array $tArgs
     * @return mixed
     */
    public function __call($funcName, $tArgs) {
        foreach($this->_tExendInstances as &$object) {
            if(method_exists($object, $funcName)) return call_user_func_array(array($object, $funcName), $tArgs);
        }
        throw new Exception("The $funcName method doesn't exist");
    }

    /**
     * Méthode magique __get()
     * On va reporter chaque lecture d'attribut (accesseur) sur une des instances des classes mères
     * @param string $varName
     * @return mixed
     */
    public function __get($varName) {
        foreach($this->_tExendInstances as &$object) {
            $tDefinedVars   = get_defined_vars($object);
            if(property_exists($object, $funcName)) return $object->{$varName};
        }
        throw new Exception("The $varName attribute doesn't exist");
    }
}

?>
