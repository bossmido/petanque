<?php
require_once(__DIR__.'/Configuration.php');
require_once(__DIR__."/fonction.php");
require_once(__DIR__."/requetes.php");


/**
 * XXX classe singleton de chargement de de BDD avec pattern adaptateur de PDO
 *
 * @author    XXX
 * @version   XXX
 * @copyright XXX
 */
class BDD {

	private $resultat;
	public static $instance;
	private $bdd;
	
	public  $requete_insertion_joueur;
	public  $requete_modification_joueur;
	public  $requete_effacer_joueur;
	public  $requete_insertion_equipe;
	public  $requete_effacer_equipe;
	public  $requete_insertion_tournoi;
	public  $requete_effacer_tournoi;
	public  $requete_insertion_partie;
	public  $requete_effacer_partie;
	public  $requete_ajout_equipe_tournoi;

	public  $chercher_equipe;
    public  $chercher_joueur;
	public  $chercher_tournoi;
	public  $chercher_score;
	public  $chercher_tout_type;
    public  $chercher_tout_joueur;
	public  $chercher_tout_tournoi;
	public  $chercher_tout_score;
	public  $chercher_tout_equipe;
	public  $chercher_equipe_tournoi;
    public  $chercher_joueur_tournoi;
	public  $chercher_joueur_tournoi_avec_partie;
		
	public  $chercher_tournoi_depuis_joueur;
    
    public $ajouter_joueur_plus_equipe_tournoi;
    public $requete_type_tournoi;
	// Associations
	// Operations
	/**
	 *
	 * @access public
	 */
	function connection($creation=true) {
                $dbname=';dbname='.Configuration::$database;
        if(!$creation){
            $dbname="";
        }
		$this->bdd = new PDO('mysql:host=127.0.0.1'.$dbname, Configuration::$login, Configuration::$mot_de_passe);
		$this->bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}

	/**
	 * XXX
	 *
	 */
	function deconnection() {
		$this->bdd = null;
	}

	function recuperer_resultat($param = array()) {
		$retour = null;
        $this->resultat = array();

        //if( $this->statement->rowCount()>1){
        try{
		    $this->resultat = $this->statement->fetchAll();
		    //var_dump($this->resultat);
		}catch(Exception $e){
		return array();
		}
		//}else{
		//    $this->resultat = array();
		//}
		$this->statement->closeCursor();
		return $this->resultat;

	}
	public function get_id(){
	    return $this->bdd->lastInsertId(); 
	}
	/**
	 * XXX
	 *
	 * @param   $param=array() XXX
	 * @access public
	 */
	function executer_requete($sql,$param = array(), $retour = false) {
	    print("\n\n\n\n\n");
	    if( empty($sql)){
    	    throw new Exception("@@@@@@@ requête vide  : ".$sql."@@@@@@@@");
	    }
	    //var_dump($sql);
	    //die;
		$this->statement = $this->bdd->prepare($sql);
		foreach ($param as $val) {
		    //var_dump($param);
		
		    if( $val['type'] == PDO::PARAM_INT ){
		        $val['value'] = intval($val['value']);
		    }
		    //var_dump($param);
		    //var_dump(":" . $val['libelle']."  ". $val['value'].$val['type']);
			$this->statement->bindParam(":" . $val['libelle'], $val['value'],$val['type']);
		}
		if (!$retour) {
			$retour = $this->statement->execute();
			//var_dump($this->statement->debugDumpParams());
			return $retour;
		} else {
			$this->statement->execute();
        	//var_dump($this->statement->debugDumpParams());
			$retour =  $this->recuperer_resultat();
			//var_dump($this->statement->debugDumpParams());
			//var_dump($sql);
			//var_dump($retour);
			return $retour;
		}
	}
	function executer_requete_retour($sql,$param = array(), $retour = false) {
	    return $this->executer_requete($sql,$param, true);
	}
	function __construct() {
		global $requete;
        
     
		$this->requete_insertion_joueur = $requete["requete_insertion_joueur"];
		$this->requete_modification_joueur = $requete["requete_modification_joueur"];
		$this->requete_effacer_joueur = $requete["requete_effacer_joueur"];
		$this->requete_insertion_equipe = $requete["requete_insertion_equipe"];
	    $this->requete_effacer_equipe = $requete["requete_effacer_equipe"];
		$this->requete_insertion_tournoi = $requete["requete_insertion_tournoi"];
		$this->requete_effacer_tournoi = $requete["requete_effacer_tournoi"];
		$this->requete_insertion_partie = $requete["requete_insertion_partie"];
		$this->requete_effacer_partie = $requete["requete_effacer_partie"];
		$this->requete_ajout_equipe_tournoi = $requete["requete_ajout_equipe_tournoi"];
		
		$this->chercher_tournoi = $requete["chercher_tournoi"];
		$this->chercher_equipe = $requete["chercher_equipe"];
		$this->chercher_joueur = $requete["chercher_joueur"];
		
     	$this->chercher_tout_tournoi = $requete["chercher_tout_tournoi"];
		$this->chercher_tout_equipe = $requete["chercher_tout_equipe"];
		$this->chercher_tout_joueur = $requete["chercher_tout_joueur"];
		$this->chercher_tout_type = $requete["chercher_tout_type"];
	
     	$this->chercher_equipe_tournoi = $requete["chercher_equipe_tournoi"];
		$this->chercher_joueur_tournoi = $requete["chercher_joueur_tournoi"];
		$this->chercher_joueur_tournoi_avec_partie = $requete["chercher_joueur_tournoi_avec_partie"];
		$this->nombre_joueur_par_equipe = $requete["nombre_joueur_par_equipe"];
		
		$this->ajouter_joueur_plus_equipe_tournoi = $requete["ajouter_joueur_plus_equipe_tournoi"];
		
	
	    $this->chercher_tournoi_depuis_joueur = $requete["chercher_tournoi_depuis_joueur"];
	    
	    $this->nombre_joueur_tournoi = $requete["nombre_joueur_tournoi"];
        $this->nombre_joueur_par_equipe = $requete["nombre_joueur_par_equipe"];
        
        $this->chercher_joueur_equipe = $requete['chercher_joueur_equipe'];
        $this->requete_effacer_tout_equipe_tournoi = $requete['requete_effacer_tout_equipe_tournoi'];
        $this->requete_type_tournoi = $requete['requete_type_tournoi'];
		$this->connection();
		$this->resultat = array();
	}
	public static $nombre_creation=0;

	/**
	 * methode du singleton qui doit être appelé à chaque foiss
	 *
	 * @access public
	 */
	public static function get_instance() {

		if(is_null(self::$instance)) {
			self::$instance = new BDD();
			self::$instance->connection();
		}
        self::$nombre_creation++;
        //vd($this->nombre_creation);
		return self::$instance;

	}

}

?>
