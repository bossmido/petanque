<?php


class RefVue{
    //resultat final
    public static $chaine_html="";
    //differentes parties de la page
    public static $chaine_contenu="";
    protected static $chaine_css= '<link rel="stylesheet" type="text/css" href="static/style.php"><link rel="stylesheet" type="text/css" href="static/jquery-ui.css">'
;
    protected static $chaine_javascript= '<script type="text/javascript" src="static/jquery.js" ></script><script type="text/javascript" src="static/script.js" ></script><script type="text/javascript" src="static/jquery-ui.min.js" ></script>';
    protected static $chaine_debug = "";
    protected static $initialized=false;
    private static function initialize()
    {
        if (self::$initialized)
            return;

        self::$initialized = true;
    }
    
    
    public static function concat_chaine_html($val)
    {
        //self::initialize();
        self::$chaine_html = self::$chaine_html  . $val;
    }
 
     public static function concat_chaine_debug($val)
    {
        //self::initialize();
        self::$chaine_debug = self::$chaine_debug  . $val;
    }
    
    public static function concat_chaine_contenu($val)
    {
        //self::initialize();
        self::$chaine_contenu = self::$chaine_contenu.$val;
    }
    
    public static function concat_chaine_css($val)
    {
        //self::initialize();
        self::$chaine_css =self::$chaine_css. $val;
    }

    
    //
    public static function get_chaine_html($val)
    {
        //self::initialize();
        self::$chaine_html .= $val;
    }
 
    public static function get_chaine_contenu($val)
    {
        //self::initialize();
        return self::$chaine_contenu;
    }
    
    public static function get_chaine_css($val)
    {
        //self::initialize();
        return self::$chaine_css;
    }
    public static function afficher_html()
    {
        print(self::$chaine_html);
    }
    
    public static function generer_html() {
    
        self::$chaine_html = str_replace('%%contenu%%',self::$chaine_contenu,self::$chaine_html); 
        RefVue::concat_chaine_html("<div class='debug'><h3>var_dump</h3>".self::$chaine_debug."</div>");
        self::$chaine_html =str_replace('%%contenu_css%%',self::$chaine_css,self::$chaine_html); 
        self::$chaine_html = str_replace('%%contenu_javascript%%',self::$chaine_javascript,self::$chaine_html);     
    }   
  
}
