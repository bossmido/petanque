<?php
require_once 'Controlleur.php';
require_once 'ControlleurTournoi.php';
/**
 * XXX detailed description
 *
 * @author    XXX
 * @version   XXX
 * @copyright XXX
 */
class ControlleurJoueur extends Controlleur {
    // Attributes
    // Associations
    // Operations
    public function get_joueur(){
            $bdd = BDD::get_instance();
            return $bdd->executer_requete_retour($bdd->chercher_tout_joueur);
    }
    
    public function get_joueur_tournoi(){
            $bdd = BDD::get_instance();
              $param_requete=array(array("value"=> $_SESSION['tournoi_courant'] ,"type"=>PDO::PARAM_INT,"libelle"=>"id_tournoi_tournoi"));
            return $bdd->executer_requete_retour($bdd->chercher_joueur_tournoi_avec_partie,$param_requete);
    }
    
    public function afficher_tableau($param=array()){
        var_dump($_SESSION);
        //$func=&RefVue::concat_chaine_contenu;
        $this->debut_form(); 
        RefVue::concat_chaine_contenu("<td><input type='hidden' value='".$_SESSION['tournoi_courant']."' name='".id_tournoi_tournoi."'>");       
        RefVue::concat_chaine_contenu(" ");
                        $liste_choix_type= array(
 array("libelle"=>"Sélectionner les joueurs à ajouter au tournoi :","name"=>"type","type"=>"radio","value"=>array(array("libelle"=>"doublette","name"=>"type","value"=>"doublette"),array("libelle"=>"triplette","name"=>"type","value"=>"triplette")),"placeholder"=>""),
array("name"=>"tirage","type"=>"submit","value"=>"Tirage !"));
        
        $this->afficher_form("POST","post.php",$liste_choix_type,"form0");            
      
        
        RefVue::concat_chaine_contenu("<table>");

        RefVue::concat_chaine_contenu("<theader>");
        foreach($param['entete'] as $head){
            RefVue::concat_chaine_contenu("<th>".$head."</th>");
        }
        RefVue::concat_chaine_contenu("</theader>");
        RefVue::concat_chaine_contenu("<tbody>");
       
        /*foreach($param['contenu']  as $key => $value) {
            if(in_array($value,$param["vrai_contenu"])){
                RefVue::concat_chaine_contenu("<td>".$param['contenu'][(string) $key]."</td>");
            }
        }*/
        $inc=1;
    foreach($param['contenu']  as $key => $value)  {
        RefVue::concat_chaine_contenu("<tr>");
        RefVue::concat_chaine_contenu("<td>".$value['nom_joueur']."  ".$param['prenom_joueur']."</td>");
        
        RefVue::concat_chaine_contenu("<td>".$value['sexe_joueur']."</td>");
          $checked="";
          foreach($param['deja_inscris'] as $key2=>$val){
            if(intval($value['id_joueur_joueur']) == intval($val['id_joueur_joueur'])){
            var_dump($value['id_joueur_joueur'] );
            var_dump($val['id_joueur_joueur'] );
                $checked="checked";
                break;
            } 
          }
        RefVue::concat_chaine_contenu("<td><input type='hidden' name='id_joueur_joueur[]' value='".$value['id_joueur_joueur']."'  /><input value='".$inc."'   ".$checked." class='checkbox_selection' type='checkbox' name='selection[]' / ></td>");
        
        RefVue::concat_chaine_contenu("</tr>");
        $inc++;
    } 
    RefVue::concat_chaine_contenu("<tr><td colspan='2'></td><td>selection tout <input  type='checkbox' value='-1' name='selectionner_tout'  id='selectionner_tout' /></td></tr>");  
   
    }
    
    public function afficher_contenu($param=array()){
               $this->afficher_titre("Joueur");
               
                //////////////////////////////////////
                //partie tirage de compétition
                //////////////////////////////////////
                
                $info_tournoi = new ControlleurTournoi();
                $info_tournoi->afficher_fragment_tournoi();
                
                //////////////////////////////////////
                //partie liste des joueurs
                //////////////////////////////////////
                //on récupère la memoire du tournoi
                $liste_joueur_dans_le_tournoi = $this->get_joueur_tournoi();
                var_dump($liste_joueur_dans_le_tournoi);
                //RefVue::concat_chaine_contenu("<form id='form1' action='post.php' method='POST'>");
                $liste_contenu=$this->get_joueur();
                //var_dump($liste_contenu);
                $this->afficher_tableau(
                    array(
                        "entete"=>array(
                            "Nom-Prénom",
                            "Sexe",
                            "Sélection")
                        ,
                        "contenu"=>
                            $liste_contenu,
                            "deja_inscris"=>
                            $liste_joueur_dans_le_tournoi
                    )
                );
                //RefVue::concat_chaine_contenu("</form>");
                //////////////////////////////////////
                // partie création de joueur
                //////////////////////////////////////
                
                
                $this->afficher_titre("Nouveau joueur :");
               
                $liste_choix= array(
                array("libelle"=>"Nom : ","name"=>"nom_joueur","type"=>"text","value"=>"","placeholder"=>""),
                array("libelle"=>"Prénom : ","name"=>"prenom_joueur","type"=>"text","value"=>"","placeholder"=>""),
                array("libelle"=>"Sexe :","name"=>"sexe","type"=>"radio","value"=>array(array("libelle"=>"M","name"=>"sexe_joueur","value"=>0),array("libelle"=>"F","name"=>"sexe_joueur","value"=>1)),"placeholder"=>""),
array("name"=>"creation_joueur","type"=>"submit","value"=>"Créer"));
               
      
                $this->afficher_form("POST",Configuration::$adresse[basename(__FILE__)],$liste_choix,"form2");
        $this->fin_form();
    }
    public function __construct(){
            $this->nom="Joueur";
    }
}

?>
