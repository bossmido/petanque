<?php
require_once __DIR__.DIRECTORY_SEPARATOR.'Controlleur.php';
require_once  __DIR__.DIRECTORY_SEPARATOR.'ModeleTournoi.php';
require_once  __DIR__.DIRECTORY_SEPARATOR.'fonction.php';
/**
 * XXX detailed description
 *
 * @author    XXX
 * @version   XXX
 * @copyright XXX
 */
class ControlleurTournoi extends Controlleur {
    // Attributes
    // Associations
    /**
     * XXX
     *
     * @var    Tournoi $unnamed
     * @access private
     * @accociation Tournoi to unnamed
     */
    #var $unnamed;

    // Operations


    
    public function get_liste_type(){
        $bdd = BDD::get_instance();
        return $bdd->executer_requete_retour($bdd->chercher_tout_type);
    }
    
    public function get_type_tournoi(){
        $bdd = BDD::get_instance();
        return $bdd->executer_requete_retour($bdd->chercher_tournoi,array(array("value"=> intval($_SESSION['tournoi_courant']) ,"type"=>PDO::PARAM_INT,"libelle"=>"id_tournoi_tournoi")));
    }
    public function get_liste_tournoi(){
        $bdd = BDD::get_instance();
        return $bdd->executer_requete_retour($bdd->chercher_tout_tournoi);
    }
    
    
    public function get_tournoi_session(){
        $bdd = BDD::get_instance();
        return $bdd->executer_requete_retour($bdd->chercher_tournoi,array(array("value"=> intval($_SESSION['tournoi_courant']) ,"type"=>PDO::PARAM_INT,"libelle"=>"id_tournoi_tournoi")));
    }
    
    public function afficher_fragment_tournoi($param=array()){
        $tournoi = $this->get_tournoi_session()[0];
        //var_dump($this->get_tournoi_session());
        //var_dump($tournoi);
        RefVue::concat_chaine_contenu("Tournoi ".$tournoi['nom_tournoi_tournoi']." à la date du ".date_db2html($tournoi['date_tournoi'])."en  parties,tirage au sort : ".$tournoi['libelle_type']);
    }
    
    
    public function afficher_contenu($param=array()){
       $liste_tournoi = $this->get_liste_tournoi();
       $id = intval($this->get_tournoi_session()[0]['id_tournoi_tournoi']);
       $type = intval($this->get_type_tournoi($id)[0]['id_partie_type']);
       var_dump($type);
       $liste_choix = array();
       $this->afficher_titre("Tournoi");
       $this->debut_form();
       $liste_choix =  array("libelle"=>"Choisissez un tournoi : ","name"=>"id_tournoi_tournoi","select"=>$id,"type"=>"select","value"=>"0","placeholder"=>"");
       
       //////////////////////////////  
       //partie choix de tournoi
       //////////////////////////////
         
       $value=array();
       foreach($liste_tournoi as $courant)
       {
           //var_dump($courant);
          // array_push($value,array($courant['nom_tournoi_tournoi']=>$courant['id_tournoi_tournoi']));
          $value[$courant['nom_tournoi_tournoi']] = $courant['id_tournoi_tournoi'];
       }
       $liste_choix['value'] = $value;
       vd($value);
       $this->afficher_form("POST",Configuration::$adresse[basename(__FILE__)],$liste_choix,"form1");
         RefVue::concat_chaine_contenu("Ou <br />Créer un tournoi : ");
         
         
         //////////////////////////////
         //partie création de tournoi
         //////////////////////////////
         
               $liste_type = $this->get_liste_type(); 
               //var_dump($liste_type);  
               $value=array();
               foreach($liste_type as $courant)
               {
                  $value[$courant['libelle_type']] = $courant['id_partie_type'];
               }
               var_dump($type);
                $liste_choix= array(
                //array("libelle"=>"","name"=>"id_tournoi_tournoi","type"=>"hidden","value"=>['id_tournoi_tournoi'],"placeholder"=>""),
                array("libelle"=>"Nom : ","name"=>"nom_tournoi_tournoi","type"=>"text","value"=>"","placeholder"=>""),
                array("libelle"=>"date : ","name"=>"date_tournoi","type"=>"text","value"=>"","placeholder"=>"","class"=>"datapicker"),
                array("libelle"=>"Nombre de partie : ","name"=>"nombre_partie_tournoi","type"=>"text","value"=>"","placeholder"=>"1"),
                array("libelle"=>"Type : ","name"=>"id_partie_type","type"=>"select","select"=>$type,"value"=>$value,"placeholder"=>""),
                                
array("name"=>"creation_tournoi","type"=>"submit","value"=>"Go"));
            
      
       //$liste_choix['value'] = $value;
                $this->afficher_form("POST",Configuration::$adresse[basename(__FILE__)],$liste_choix,"form2");
              $this->fin_form();
    }
    
    public function __construct(){
            $this->nom="Tournoi";
    }
}

?>
