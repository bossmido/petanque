<?php
require_once __DIR__."/Configuration.php";
require_once __DIR__."/RefVue.php";
require_once __DIR__."/BDD.php";

function vd($param){
    if(Configuration::$debug){
     
        ob_start();
        print_r($param);
        $message_debug = ob_get_contents();
        ob_end_clean();

        RefVue::concat_chaine_debug("<br />  fichier : ".basename(debug_backtrace()[0]['file'])."  ligne : ".debug_backtrace()[0]['line']."  <span style='color : red;'>".$message_debug."</span>");
        //print_r($param);
    }
}

function date_db2html($d) {
	if ($d) {
		return substr($d, 8, 2) . '/' . substr($d, 5, 2) . '/' . substr($d, 0, 4);
	}
}
function date2db($d) {
	if ($d) {
		$date = substr($d, 6, 4) . '-' . substr($d, 3, 2) . '-' . substr($d, 0, 2);
		return $date;
	} else {
		return null;
	}
}


function datemysql($datetime) {
    return dateFR($datetime,true);
}

function callback_to_str_affichage_td($param){
        RefVue::concat_chaine_contenu(RefVue::chaine_contenu."</tr>");
        foreach($param as $val){
            RefVue::concat_chaine_contenu("<td>");
            RefVue::concat_chaine_contenu($val);
            RefVue::concat_chaine_contenu("</td>");
        }
        
        RefVue::concat_chaine_contenu("</tr>");
}


function liste_table($nom_table){
    $requete = "SHOW TABLES;";
    Configuration::$liste_entree_table = executer_requete($requete,array(),$retour=true);
}


function liste_libelle($nom_table){
    $requete = "DESCRIBE TABLE :nom_table;";
    Configuration::$liste_entree_table[$nom_table] = executer_requete(array("nom_table"=>$nom_table),true);
}

function liste_tous_les_libelles(){
    $requete = "DESCRIBE TABLE :nom_table;";
    foreach( Configuration::$liste_complete_des_table  as $val){
        Configuration::$liste_entree_tables[$nom_table.""]  = executer_requete($requete,array("libelle"=>"nom_table", "value"=>$val),$retour=true);
    }
}
?>
