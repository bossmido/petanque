<?php
require_once 'Controlleur.php';
require_once 'ControlleurTournoi.php';

/**
 * XXX detailed description
 *
 * @author    XXX
 * @version   XXX
 * @copyright XXX
 */
class ControlleurTirage extends Controlleur
{
    public function get_liste_equipe($id){
        $bdd = BDD::get_instance();
        return $bdd->executer_requete_retour($bdd->chercher_equipe_tournoi,
            array(
                array(
                "value"  => $id ,
                "type"   => PDO::PARAM_INT,
                "libelle"=> "id_tournoi_tournoi"
                )
            )
        );

    }
    public function get_nombre_joueur_par_equipe(){
        $bdd = BDD::get_instance();
        $param_requete=array(array("value"=> $_SESSION['tournoi_courant'] ,"type"=>PDO::PARAM_INT,"libelle"=>"id_tournoi_tournoi"));
        return $bdd->executer_requete_retour($bdd->nombre_joueur_par_equipe,$param_requete);
    }

    public function get_joueur_equipe($id){
        $bdd = BDD::get_instance();
        $equipe =  $bdd->executer_requete_retour($bdd->chercher_joueur_equipe,array(array("value"=> $id,"type"=>PDO::PARAM_INT,"libelle"=>"id_equipe_equipe")));
        return $equipe;
    }
        
    public function get_tournoi_courant(){
        $bdd = BDD::get_instance();
        $retour = $bdd->executer_requete_retour($bdd->chercher_tournoi,array(array("value"=> intval($_SESSION['tournoi_courant']) ,"type"=>PDO::PARAM_INT,"libelle"=>"id_tournoi_tournoi")));
        return $retour;
    }  
    
    protected function __verifier_nouveau_tirage($param=array(),$niveau=0,$duel1,$duel2){
       
        for($j=($niveau-1); $j>=0 ; $j--){
        
            for($i=0; $i<(count($param[$j])-1); $i++ ){
                $retour=true;
                //var_dump($param[$j]);
                if(($param[$j][$i]['id_equipe_equipe'] == $duel1['id_equipe_equipe'] and $param[$j][$i+1]['id_equipe_equipe'] == $duel2['id_equipe_equipe']) or ($param[$j][$i]['id_equipe_equipe'] == $duel2['id_equipe_equipe'] and $param[$j][$i+1]['id_equipe_equipe'] == $duel1['id_equipe_equipe'])  ){
                    var_dump("false");
                    return false;
                }
            }
        }
        var_dump("true");
        return true;
    }
    protected function __tirage(){
        $tournoi = $this->get_tournoi_courant();     
        $nombre_duel = intval($tournoi[0]['nombre_partie_tournoi']);
        $liste_equipe = $this->get_liste_equipe(intval($tournoi[0]['id_tournoi_tournoi']));
        array_rand($liste_equipe);

        $liste_attribue = array(array(),array(),array());
        for($j=0; $j< $nombre_duel ; $j++){
            array_rand($liste_equipe);
            for($i = 0; $i < (count($liste_equipe) - 1) ; $i++){
                array_push($liste_attribue[$j],array($liste_equipe[$i],$liste_equipe[$i+1]));
            }
        }

         return $liste_attribue;
    }
    protected function __tirage_successif(){
        $tournoi = $this->get_tournoi_courant();
        $nombre_duel = $tournoi[0]['nombre_partie_tournoi'];
        $liste_equipe = $this->get_liste_equipe(intval($tournoi[0]['id_tournoi_tournoi']));
        array_rand($liste_equipe);

        $liste_attribue = array(array(),array(),array());
        
        ////////////////////////////////////////
        // calcul de la mélée coeur du projet
        ////////////////////////////////////////
        
        //var_dump($liste_equipe);
        for($j=0; $j< $nombre_duel ; $j++){
            array_rand($liste_equipe);
            //var_dump(count($liste_equipe));
            for($i=0; $i < (count($liste_equipe)-1); $i++ ){
             
                if($this->__verifier_nouveau_tirage($liste_attribue,$j,$liste_equipe[$i],$liste_equipe[$i+1])){
                /*
                var_dump("@@@@@@@@@@@@@@@@@@@@@@@");
                var_dump("avant :",$liste_attribue[$j]);
                                var_dump("inserer");
                    array_push($liste_attribue[$j],array($liste_equipe[$i],$liste_equipe[$i+1]));
                var_dump("apres :",$liste_attribue[$j]);
                var_dump("@@@@@@@@@@@@@@@@@@@@@@@");
                */
                }else{
                   for($k=0; $k < ((count($liste_equipe)-1)) ; $k++){
                        $inc_courant = $k%(count($liste_equipe)-1);

                        if($this->__verifier_nouveau_tirage($liste_attribue,$j,$liste_equipe[$i],$liste_equipe[$i+1])){
                            array_push($liste_attribue[$j],array($liste_equipe[$i],$liste_equipe[$inc_courant]));
                        }
                   }
                }
                }
           
            //
         }
        var_dump($liste_attribue);
         return $liste_attribue;
    }
    
    public function __correspondance_joueur($param=array()){
        var_dump($param);
        $tournoi = $this->get_tournoi_courant();
        $nombre_duel = $tournoi[0]['nombre_partie_tournoi'];
        $nombre_joueur = intval($this->get_nombre_joueur_par_equipe()[0]['compteur']);
         $liste_sortie=array();
         for($j=0; $j<  (count($param)); $j++){
            $liste_sortie[$j]=array();
            for($i=0; $i<(count($param[$j])-1); $i++ ){
                $liste_sortie[$j][$i]=array();
                for($k=0; $k<(count($param[$j][$i])); $k++ ){
                    
                    $joueur = $this->get_joueur_equipe(intval($param[$j][$i][$k]['id_equipe_equipe']));    
                    var_dump($j);
                    var_dump($i);
                    var_dump($k); 
                    var_dump($param[$j][$i][$k]['id_equipe_equipe']);      
                    var_dump($joueur);
                    $liste_sortie[$j][$i][$k]=$joueur;
                }
            } 
          }
          return $liste_sortie;
    }
    
    public function __get_duel_td($param=array()){
           $nombre = intval($this->get_tournoi_courant()[0]['nombre_partie_tournoi']);
           var_dump($nombre);
           $chaine_td_sortie=array();
           $chaine_td_sortie="";
           for($j=0; $j< $nombre ; $j++){
             for($i=0; $i<(count($param[$j])); $i++ ){
                             $indice_algo=0;
                if(count($param[$j][$i][0])>count($param[$j][$i][1])){
                               $indice_algo=count($param[$j][$i][0]);
                }else{
                                $indice_algo=count($param[$j][$i][1]);
                }
                
                for($k=0; $k<$indice_algo; $k++ ){
                //on arrive à l'opposition jouer à un à un en gardant le versus équipe
                //on test si l'equipe à quel'un contre qui joué

                var_dump($param[$j][$i]);
                    //for($l=0; $l<(count($param[$j][$k])-1); $l++ ){
                    $chaine_td_sortie = $chaine_td_sortie ."<tr><td>Partie ".($j+1)." </td><td>".$param[$j][$i][0][$k]['nom_joueur']."</td><td>".$param[$j][$i][0][$k]['prenom_joueur']."</td><td></td><td>".$param[$j][$i][1][$k]['nom_joueur']."</td><td>".$param[$j][$i][1][$k]['prenom_joueur']."</td><td><input value='score' type='submit' name='score_".($i+1)."' /></td></tr>";
                    //}
                }
             }
           }
           return $chaine_td_sortie;
    }
    public function afficher_contenu($param=array()){
           
           $this->afficher_titre("Tirage");

                //////////////////////////////////////
                //partie tirage de compétition
                //////////////////////////////////////

                $info_tournoi = new ControlleurTournoi();
                $info_tournoi->afficher_fragment_tournoi();
                
                //////////////////////////////////////
                //partie liste des joueurs
                //////////////////////////////////////
                
                ini_set("xdebug.var_display_max_depth ",5);
               
                $liste_duel_equipe=$this->__tirage();
                //var_dump($liste_duel_equipe);
                $liste_duel_joueur=$this->__correspondance_joueur($liste_duel_equipe);
                //var_dump($liste_duel_equipe[0]);
                $liste_contenu=$this->__get_duel_td($liste_duel_joueur);
                //var_dump($liste_contenu);
                $this->afficher_tableau(
                    array(
                        "entete"=>array(
                            "  ",
                            "Nom",
                            "Prénom",
                            "Contre",
                            "Nom",
                            "Prénom",
                            " "
                            )
                        ,
                        "contenu"=>array(
                            $liste_contenu
                        )
                    )
                );
    }
    
    public function __construct(){
            $this->nom="Tirage";
    }
}

?>
