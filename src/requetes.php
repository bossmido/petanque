<?php 

require_once __DIR__.DIRECTORY_SEPARATOR."fonction.php";
global $requete;

$requete=array(
"requete_insertion_joueur"=>
"INSERT INTO joueur(nom_joueur,prenom_joueur,sexe_joueur)  VALUES(:nom_joueur,:prenom_joueur,:sexe_joueur);",
"requete_modification_joueur"=>
"UPDATE joueur SET WHERE joueur_id = :joueur_id;",
"requete_effacer_joueur"=>
"DELETE FROM joueur WHERE joueur_id = :joueur_id;",
"requete_insertion_equipe"=>
"INSERT INTO equipe(nom_equipe)  VALUES(:nom_equipe);",
"requete_effacer_equipe"=>
"DELETE FROM equipe WHERE equipe_id = :equipe_id;",
"requete_insertion_tournoi"=>
"INSERT INTO tournoi(nombre_partie_tournoi,date_tournoi,nom_tournoi_tournoi,id_partie_type)  VALUES(:nombre_partie_tournoi,:date_tournoi,:nom_tournoi_tournoi,:id_partie_type);",
"requete_effacer_tournoi"=>
"DELETE FROM tournoi WHERE id_tournoi_tournoi = :id_tournoi_tournoi;",
"requete_insertion_partie"=>
"INSERT INTO partie()  VALUES();",
"requete_effacer_partie"=>
"DELETE FROM partie WHERE partie_id = :partie_id;",
"requete_ajout_equipe_tournoi"=>
"",
//partie recherche par id
"chercher_joueur"=>
"SELECT *  FROM joueur WHERE joueur.joueur_id = :joueur_id;",
"chercher_tournoi"=>
"
SELECT *  FROM tournoi 
LEFT JOIN type as t ON t.id_partie_type = tournoi.id_partie_type
WHERE tournoi.id_tournoi_tournoi = :id_tournoi_tournoi;",
"chercher_equipe"=>
"SELECT *  FROM equipe WHERE joueur.equipe_id = :equipe_id  ;",
//partie recherche sans condition
"chercher_tout_joueur"=>
"SELECT *  FROM joueur;",
"chercher_tout_tournoi"=>
"SELECT *  FROM tournoi;",
"chercher_tout_equipe"=>
"SELECT *  FROM equipe;",
"chercher_tout_type"=>
"SELECT *  FROM type;",

//requete complexe

//recherche depuis tournoi
"chercher_equipe_tournoi"=>
"
SELECT * FROM tournoi
lEFT JOIN participe_à as p  ON p.id_tournoi_tournoi =  tournoi.id_tournoi_tournoi
lEFT JOIN equipe as e  ON e.id_equipe_equipe =  p.id_equipe_equipe 
lEFT JOIN equipe_joueur as ej   ON ej.id_equipe_equipe =  e.id_equipe_equipe 
lEFT JOIN joueur as j  ON j.id_joueur_joueur =  ej.id_joueur_joueur
WHERE tournoi.id_tournoi_tournoi= :id_tournoi_tournoi
GROUP BY e.id_equipe_equipe
;"
,
"chercher_equipe_tournoi"=>
"
SELECT * FROM tournoi
lEFT JOIN participe_à as p  ON p.id_tournoi_tournoi =  tournoi.id_tournoi_tournoi
lEFT JOIN equipe as e  ON e.id_equipe_equipe =  p.id_equipe_equipe 
lEFT JOIN equipe_joueur as ej   ON ej.id_equipe_equipe =  e.id_equipe_equipe 
lEFT JOIN joueur as j  ON j.id_joueur_joueur =  ej.id_joueur_joueur
WHERE tournoi.id_tournoi_tournoi = :id_tournoi_tournoi
;",
"chercher_joueur_tournoi"=>
"
SELECT * FROM tournoi
lEFT JOIN participe_à as p  ON p.id_tournoi_tournoi =  tournoi.id_tournoi_tournoi
lEFT JOIN equipe as e  ON e.id_equipe_equipe =  p.id_equipe_equipe 
lEFT JOIN equipe_joueur as ej   ON ej.id_equipe_equipe =  e.id_equipe_equipe 
lEFT JOIN joueur as j  ON j.id_joueur_joueur =  ej.id_joueur_joueur
WHERE tournoi.id_tournoi_tournoi = :id_tournoi_tournoi;
",
"chercher_joueur_tournoi_avec_partie"=>
"
SELECT * FROM tournoi
lEFT JOIN participe_à as p  ON p.id_tournoi_tournoi =  tournoi.id_tournoi_tournoi
lEFT JOIN equipe as e  ON e.id_equipe_equipe =  p.id_equipe_equipe 
lEFT JOIN equipe_joueur as ej   ON ej.id_equipe_equipe =  e.id_equipe_equipe 
lEFT JOIN joueur as j  ON j.id_joueur_joueur =  ej.id_joueur_joueur
WHERE tournoi.id_tournoi_tournoi = :id_tournoi_tournoi;
",
"chercher_joueur_tournoi_avec_partie"=>
"
SELECT * FROM tournoi
lEFT JOIN participe_à as p  ON p.id_tournoi_tournoi =  tournoi.id_tournoi_tournoi
lEFT JOIN equipe as e  ON e.id_equipe_equipe =  p.id_equipe_equipe 
lEFT JOIN equipe_joueur as ej   ON ej.id_equipe_equipe =  e.id_equipe_equipe 
lEFT JOIN joueur as j  ON j.id_joueur_joueur =  ej.id_joueur_joueur
lEFT JOIN partie as pa  ON pa.id_partie_Entite = p.id_partie_Entite
WHERE tournoi.id_tournoi_tournoi = :id_tournoi_tournoi;
",

//recherche depuis joueur
"chercher_tournoi_depuis_joueur"=>
"
SELECT * FROM tournoi
lEFT JOIN participe_à as p  ON p.id_tournoi_tournoi =  tournoi.id_tournoi_tournoi
lEFT JOIN equipe as e  ON e.id_equipe_equipe =  p.id_equipe_equipe 
lEFT JOIN equipe_joueur as ej   ON ej.id_equipe_equipe =  e.id_equipe_equipe 
lEFT JOIN joueur as j  ON j.id_joueur_joueur =  ej.id_joueur_joueur
lEFT JOIN partie as pa  ON pa.id_partie_Entite = p.id_partie_Entite
WHERE tournoi.id_tournoi_tournoi = :id_tournoi_tournoi;
",
//page joueur TODO
/*
UPDATE equipe_joueur
SET  id_equipe_equipe = :equipe_id_equipe_equipe
WHERE id_joueur_joueur = :id_joueur_joueur*/
"ajouter_joueur_plus_equipe_tournoi"=>"
insert into equipe_joueur (id_joueur_joueur,id_equipe_equipe) values(:id_joueur_joueur,:equipe_id_equipe_equipe);
insert into participe_à (id_equipe_equipe,id_tournoi_tournoi) values(:id_equipe_equipe,:id_tournoi_tournoi);
",
//nombre_par_equipe
"nombre_joueur_tournoi"=>"
SELECT SUM(test.compteur)
FROM (
SELECT COUNT(*) as compteur, j.id_joueur_joueur
FROM tournoi
lEFT JOIN participe_à as p  ON p.id_tournoi_tournoi = tournoi.id_tournoi_tournoi
lEFT JOIN equipe as e  ON e.id_equipe_equipe =  p.id_equipe_equipe 
lEFT JOIN equipe_joueur as ej   ON ej.id_equipe_equipe =  e.id_equipe_equipe 
lEFT JOIN joueur as j  ON j.id_joueur_joueur =  ej.id_joueur_joueur
WHERE tournoi.id_tournoi_tournoi = :id_tournoi_tournoi
GROUP BY j.id_joueur_joueur   HAVING id_joueur_joueur is not NULL
) test
;
",
"chercher_joueur_equipe"=>"
SELECT *
FROM joueur as j
lEFT JOIN equipe_joueur as ej   ON ej.id_joueur_joueur =  j.id_joueur_joueur 
lEFT JOIN equipe as e  ON e.id_equipe_equipe =  ej.id_equipe_equipe 
WHERE e.id_equipe_equipe = :id_equipe_equipe
;",
"nombre_joueur_par_equipe"=>"
SELECT test.compteur
FROM (
SELECT COUNT(*) as compteur, j.id_joueur_joueur
FROM tournoi
lEFT JOIN participe_à as pe  ON pe.id_tournoi_tournoi = tournoi.id_tournoi_tournoi
lEFT JOIN equipe as e  ON e.id_equipe_equipe =  pe.id_equipe_equipe 
lEFT JOIN equipe_joueur as ej   ON ej.id_equipe_equipe =  e.id_equipe_equipe 
lEFT JOIN joueur as j  ON j.id_joueur_joueur =  ej.id_joueur_joueur
WHERE tournoi.id_tournoi_tournoi = 1
GROUP BY ej.id_equipe_equipe   HAVING id_joueur_joueur is not NULL
) test
LIMIT 1
;",
"requete_effacer_tout_equipe_tournoi"=>
"
DELETE FROM equipe_joueur
WHERE id_equipe_equipe IN
(SELECT id_equipe_equipe
from participe_à   
WHERE participe_à.id_tournoi_tournoi =  :id_tournoi_tournoi 
 )
;",
"requete_type_tournoi"=>
"
SELECT type.id_partie_type
FROM tournoi, type
WHERE tournoi.id_tournoi_tournoi =  :id_tournoi_tournoi AND   tournoi.id_partie_type =  type.id_partie_type
;"
);
global $contenu;



?>

