DROP DATABASE IF  EXISTS   petanque;
CREATE DATABASE IF NOT EXISTS  petanque;USE petanque;

DROP TABLE IF EXISTS joueur ;
CREATE TABLE joueur (id_joueur_joueur INT(32) AUTO_INCREMENT NOT NULL,
nom_joueur VARCHAR(32),
prenom_joueur VARCHAR(32),
sexe_joueur ENUM('FEMININ','MASCULIN'),
PRIMARY KEY (id_joueur_joueur)) ENGINE=InnoDB;

DROP TABLE IF EXISTS equipe ;
CREATE TABLE equipe (id_equipe_equipe INT(32) AUTO_INCREMENT NOT NULL,
nom_equipe VARCHAR(32),
PRIMARY KEY (id_equipe_equipe)) ENGINE=InnoDB;

DROP TABLE IF EXISTS tournoi ;
CREATE TABLE tournoi (id_tournoi_tournoi INT(32) AUTO_INCREMENT NOT NULL,
nombre_partie_tournoi INT(32),
date_tournoi DATE,
nom_tournoi_tournoi VARCHAR(32),
id_partie_type INT(32),
PRIMARY KEY (id_tournoi_tournoi)) ENGINE=InnoDB;

DROP TABLE IF EXISTS type ;
CREATE TABLE type (id_partie_type INT(32) AUTO_INCREMENT NOT NULL,
libelle_type VARCHAR(32),
PRIMARY KEY (id_partie_type)) ENGINE=InnoDB;

DROP TABLE IF EXISTS partie ;
CREATE TABLE partie (id_partie_Entite INT(32) AUTO_INCREMENT NOT NULL,
score_1_Entite INT(32),
score_2_Entite INT(32),
PRIMARY KEY (id_partie_Entite)) ENGINE=InnoDB;

DROP TABLE IF EXISTS equipe_joueur ;
CREATE TABLE equipe_joueur (id_joueur_joueur INT(32) AUTO_INCREMENT NOT NULL,
id_equipe_equipe INT(32) NOT NULL,
PRIMARY KEY (id_joueur_joueur,
 id_equipe_equipe)) ENGINE=InnoDB;

DROP TABLE IF EXISTS participe_à ;
CREATE TABLE participe_à (id_equipe_equipe INT(32) AUTO_INCREMENT NOT NULL,
id_tournoi_tournoi INT(32) NOT NULL,
id_partie_Entite INT(32) NOT NULL,
PRIMARY KEY (id_equipe_equipe,
 id_tournoi_tournoi,
 id_partie_Entite)) ENGINE=InnoDB;

ALTER TABLE tournoi ADD CONSTRAINT FK_tournoi_id_partie_type FOREIGN KEY (id_partie_type) REFERENCES type (id_partie_type);

ALTER TABLE equipe_joueur ADD CONSTRAINT FK_equipe_joueur_id_joueur_joueur FOREIGN KEY (id_joueur_joueur) REFERENCES joueur (id_joueur_joueur);
ALTER TABLE equipe_joueur ADD CONSTRAINT FK_equipe_joueur_id_equipe_equipe FOREIGN KEY (id_equipe_equipe) REFERENCES equipe (id_equipe_equipe);
ALTER TABLE participe_à ADD CONSTRAINT FK_participe_à_id_equipe_equipe FOREIGN KEY (id_equipe_equipe) REFERENCES equipe (id_equipe_equipe);
ALTER TABLE participe_à ADD CONSTRAINT FK_participe_à_id_tournoi_tournoi FOREIGN KEY (id_tournoi_tournoi) REFERENCES tournoi (id_tournoi_tournoi);
ALTER TABLE participe_à ADD CONSTRAINT FK_participe_à_id_partie_Entite FOREIGN KEY (id_partie_Entite) REFERENCES partie (id_partie_Entite);

ALTER TABLE participe_à
DROP FOREIGN KEY FK_participe_à_id_partie_Entite;
